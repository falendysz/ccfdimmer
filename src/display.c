#include "dimmer.h"

extern u08 leds[3];
extern u08 blink_leds[3];
extern u08 last_buttons[4]; //*****************************TAKE OUT
extern u08 blink_value;
extern u08 hold_LED;
extern u08 system_state;
extern u08 return_state;
extern u08  current_output_scene; // take this out- just for testing!
extern struct preview_status preview;
extern struct scene live_scene, preview_xfade_scene, xfade_scene;
extern struct scene saved_scenes[NUMBER_OF_SCENES];
extern struct sequence saved_sequences[NUMBER_OF_SEQUENCES];


////////// Shift out LED values //////////
void DisplayDriver( void )
{
  struct scene* output_scene;
  u08 asm_dummy;
  u08 i, bit, ones;

  //--- First see if we're previewing anything ---
  //--- and set the appropriate output scene   ---
  switch( preview.mode ){
  case PREVIEW_MODE_OFF:
    output_scene = &(live_scene);
    break;
  case PREVIEW_MODE_SCENE:
    output_scene = &(saved_scenes[current_output_scene]);//preview.scene_index]);
    break;
  case PREVIEW_MODE_SEQUENCE:
    // this is a complicated line.  
    // we want to point at a saved scene in the end,
    // but we get the scene by looking at which sequence 
    // we're previewing, and which scene from that sequence
    // is currently being output.
    output_scene = &(saved_scenes[ (saved_sequences[preview.sequence_index].scene_queue[preview.scene_index])]);
    break;
  case PREVIEW_MODE_XFADE:
    output_scene = &(xfade_scene);
    break;
  }
  
  //--- Do the Shift!!! ---
  /*
  //--- output always live leds ---
  ones = (leds[ALWAYS_LIVE_LEDS] & ~blink_leds[ALWAYS_LIVE_LEDS]) | (blink_value & leds[ALWAYS_LIVE_LEDS]) ;

  for(bit=0; bit<NUM_ALWAYS_LIVE_LEDS; bit++){
    if (ones % 2)
      LED_PORT |= LED_DATA;
    else
      LED_PORT &= ~LED_DATA;
    LED_PORT |= LED_CLK;
    LED_PORT &= ~LED_CLK;
    ones = ones >> 1;
  }
  */
  
  //--- output fader bar graphs ---
  for(i=0; i<NUMBER_OF_FADERS; i++){
    
    ones = output_scene->faders[i];
    
    // divide the fader value by 23 to quantize
    // 0-255 to 0-10 for output to led bars
    // in FOUR INSTRUCTIONS i might add :)
    // in your FACE avr-gcc
    asm volatile (
		  "ldi %1,0x0B\n\t"
		  "mul %2,%1\n\t"
		  "mov %0,r1\n\t"
		  "clr r1\n\t"
		  : "=d" (ones), "=r" (asm_dummy)
		  : "0" (ones)
		  );
    
    // now ones holds the quantized value (0-10)
    // for the fader associated with the bar graph.
    // We should shift out that many ones and then
    // the rest of the 10 leds should be zeros.
    
    for(bit=0; bit<10; bit++)
      {
	// output the bit
	if(bit < ones)
	  LED_PORT |= LED_DATA;
	else
	  LED_PORT &= ~LED_DATA;
	
	// clock the chips
	// propogation delay is in ns range
	// so no problem.
	LED_PORT |= LED_CLK;
	LED_PORT &= ~LED_CLK;
      }
  }

  //--- output fader mute leds ---
  // red first then green for each
  // mute led
  ones = system_state;//output_scene->faders_mute;

  if( return_state == SCENE || system_state >= PREVIEW_INIT )
    // if it's in scene mode or preview mode,
    // make all the lights red
    for(i=0; i<NUMBER_OF_SCENES; i++){
      
      // make the output based on blink for red led
      if( (ones % 2) && (blink_value != 0) )
	LED_PORT |= LED_DATA;
      else
	LED_PORT &= ~LED_DATA;

      //clock
      LED_PORT |= LED_CLK;
      LED_PORT &= ~LED_CLK;

      // always output 0 to green
      LED_PORT &= ~LED_DATA;

      //clock
      LED_PORT |= LED_CLK;
      LED_PORT &= ~LED_CLK;

      ones = ones >> 1;
    }
  else
    // otherwise we're in live mode, so
    // make all the led's green
    for(i=0; i<NUMBER_OF_SCENES; i++){

      // always output 0 to red
      LED_PORT &= ~LED_DATA;
      
      //clock
      LED_PORT |= LED_CLK;
      LED_PORT &= ~LED_CLK;
      
      // make the output based on blink for green led
      if( (ones % 2) && (blink_value != 0) )
	LED_PORT |= LED_DATA;
      else
	LED_PORT &= ~LED_DATA;
      //clock
      LED_PORT |= LED_CLK;
      LED_PORT &= ~LED_CLK;

      ones = ones >> 1;
    }
  
  //--- output house leds ---
  // red first then green for each
  // house led
  ones = output_scene->house;

  if( return_state == SCENE || system_state >= PREVIEW_INIT )
    // if it's in scene mode or preview mode,
    // make all the lights red
    for(i=0; i<NUMBER_OF_HOUSE_CTLS; i++){
      
      // make the output based on blink for red led
      if( ones % 2 )
	LED_PORT |= LED_DATA;
      else
	LED_PORT &= ~LED_DATA;

      //clock
      LED_PORT |= LED_CLK;
      LED_PORT &= ~LED_CLK;

      // always output 0 to green
      LED_PORT &= ~LED_DATA;

      //clock
      LED_PORT |= LED_CLK;
      LED_PORT &= ~LED_CLK;

      ones = ones >> 1;
    }
  else
    // otherwise we're in live mode, so
    // make all the led's green
    for(i=0; i<NUMBER_OF_HOUSE_CTLS; i++){

      // always output 0 to red
      LED_PORT &= ~LED_DATA;
      
      //clock
      LED_PORT |= LED_CLK;
      LED_PORT &= ~LED_CLK;
      
      // make the output based on blink for green led
      if( ones % 2 )
	LED_PORT |= LED_DATA;
      else
	LED_PORT &= ~LED_DATA;
      //clock
      LED_PORT |= LED_CLK;
      LED_PORT &= ~LED_CLK;

      ones = ones >> 1;
    }

  //--- output relays and hold led ---
  for(i=0;i<8;i++)
    {
      if(i == 6)
	LED_PORT |= (~hold_LED & LED_DATA);
      else
	LED_PORT &= ~LED_DATA;
      LED_PORT |= LED_CLK;
      LED_PORT &= ~LED_CLK;
    }

  //--- output sequence leds ---
  ones = (leds[SEQUENCE_LEDS] & ~blink_leds[SEQUENCE_LEDS]) | (blink_value & leds[SEQUENCE_LEDS]) ;

  for(bit=0; bit<NUMBER_OF_SEQUENCES; bit++){
    if (ones % 2)
      LED_PORT |= LED_DATA;
    else
      LED_PORT &= ~LED_DATA;
    LED_PORT |= LED_CLK;
    LED_PORT &= ~LED_CLK;
    ones = ones >> 1;
  }
   
  //--- output mode/set/preview leds ---
  ones = (leds[ALWAYS_LIVE_LEDS]);
 
  for(bit=0; bit<4; bit++){
    if(ones % 2)
      LED_PORT |= LED_DATA;
    else
      LED_PORT &= ~LED_DATA;
    LED_PORT |= LED_CLK;
    LED_PORT &= ~LED_CLK;
    ones = ones >> 1;
  }
 
  //--- output scene leds ---
  ones = (leds[SCENE_LEDS] & ~blink_leds[SCENE_LEDS]) | (blink_value & leds[SCENE_LEDS]) ;

  for(bit=0; bit<NUMBER_OF_SCENES; bit++){
    if (ones % 2)
      LED_PORT |= LED_DATA;
    else
      LED_PORT &= ~LED_DATA;
    LED_PORT |= LED_CLK;
    LED_PORT &= ~LED_CLK;
    ones = ones >> 1;
  }

  // for SOME MAGIC REASON, this has to be set back to 0 or everything goes nuts.  No idea why.
  // Please figure it out for me!!!
  LED_PORT &= ~LED_DATA;
  // toggle the shift-to-display bit to move data from shift registers to the LEDs
  LED_PORT ^= 0x10;
  LED_PORT ^= 0x10;

}
