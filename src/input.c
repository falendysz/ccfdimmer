#include "dimmer.h"


////////// Variable Definitions //////////

//--- For button polling ---
u08 buttons[4];
u08 last_buttons[4];

//--- For tap tempo interrupt ---
u16 tap_ctr      = 0;
u08 tap_debounce_target = SAFE_VAL;

//--- For tap tempo led blinking ---
u16 hold_ctr     = 0;
u16 hold_time    = 500;
u08 hold_LED     = 0;  // not needed eventually, just for testing

//--- For setting the transition time
extern u08 transition_speed;

//--- For rotary encoder interrupt ---
u08 encoder_debounce_target = SAFE_VAL;

//--- Scenes and Sequences ---
extern struct scene live_scene, xfade_scene;
extern struct scene saved_scenes[NUMBER_OF_SCENES];
extern struct sequence saved_sequences[NUMBER_OF_SEQUENCES];

//--- For global ms interrupt/count ---
extern u08 ms_ctr;

//--- For global ms interrupt/count ---
u08 adc_index = 0;

////////// Read buttons into buffer //////////
u08 ScanButtons()
{

  u08 j, this_read, select;
  u08 btn0=0, btn1=0, btn2=0, btn3=0;

  //#ifdef TESTMODE
  //PORTA |= 0x10; // for testing speed
  //#endif

  //last = last_buttons[i];
  //temp_button = 0;
  //temp_last = 0;      
  
  for ( j=0; j<8; j++ )
    {
      select = BTN_SELECT;
      select &= 0xF4; // clear bits 0, 1, and 3
      select |= ((select & 0xFC) | (j & 0x03));
      if (j > 3)
	select |= 0x08;
      BTN_SELECT = select;

      __asm__ __volatile__("nop");
      __asm__ __volatile__("nop");
      __asm__ __volatile__("nop");
      __asm__ __volatile__("nop");
      __asm__ __volatile__("nop");
      __asm__ __volatile__("nop");

      this_read = BTN_READ;

      if((this_read & 0x10))
	{
	  if( !(last_buttons[0]&(1<<j)) ) // there was a 0->1 trans on this btn
	    {
	      btn0 |= (1 << j);
	    }
	  last_buttons[0] |= (1 << j);
	}
      else
	last_buttons[0] &= ~(1 << j);
      if((this_read & 0x20))
	{
	  if( !(last_buttons[1]&(1<<j)) ) // there was a 0->1 trans on this btn
	    {
	      btn1 += 1 << j;
	    }
	  last_buttons[1] |= (1 << j);
	}
      else
	last_buttons[1] &= ~(1 << j);
      if((this_read & 0x40))
	{
	  if( !(last_buttons[2]&(1<<j)) ) // there was a 0->1 trans on this btn
	    {
	      btn2 += 1 << j;
	    }
	  last_buttons[2] |= (1 << j);
	}
      else
	last_buttons[2] &= ~(1 << j);
      if((this_read & 0x80))
	{
	  if( !(last_buttons[3]&(1<<j)) ) // there was a 0->1 trans on this btn
	    {
	      btn3 += 1 << j;
	    }
	  last_buttons[3] |= (1 << j);
	}
      else
	last_buttons[3] &= ~(1 << j); 
    }
     
  buttons[0] = btn0;
  buttons[1] = btn1;
  buttons[2] = btn2;
  buttons[3] = btn3;
  
  // return 1 if any button pressed
  return (btn0 || btn1 || btn2 || btn3); 
}



ISR(ADC_vect) // ADC Conversion Complete Interrupt Handler
{
  // ADCH is where the top 8 bits of the result will be
  //PORTC = (~ADCL | 0x3F);
  //if(ADCL & 0x80)     // this prevents dithering near LSB boundaries
  //live_scene.faders[0] = ADCH;
  switch(adc_index)
    {
    case 0:
    case 1:
    case 2:
      live_scene.faders[adc_index] = ADCH;
      ADMUX = 0x60;
      PORTC &= 0x3F;
      PORTC |= (0xC0 & ((adc_index+1) << 6));
      break;
    case 3:
    case 4:
    case 5:
    case 6:
      live_scene.faders[adc_index] = ADCH;
      ADMUX = 0x61;
      PORTC &= 0x3F;
      PORTC |= (0xC0 & ((adc_index+1) << 6));
      break;
    case 7:
      live_scene.faders[adc_index] = ADCH;
      ADMUX = 0x62;      
      break;
    case 8:
      transition_speed = (ADCH >> 3);
      ADMUX = 0x60;
      PORTC &= 0x3F;
      break;
    default:
      adc_index = 0;
      break;
    }
  //else                // The high byte must be read at least once
  //  dummy = ADCH;     // after reading the low byte (datasheet pg. 224)
  adc_index++;
  adc_index %= 9;
}


// interrupt0: rotary encoder
ISR(INT0_vect) // External Interrupt 0 (rotary encoder)
{


#ifdef __AVR_ATmega16__
  GICR &= ~0x40; //disable INT0 Interrupt
#endif
#ifdef __AVR_ATmega164P__
  EIMSK &= ~0x01;  // disnable INT0 Interrupt
#endif

  if(PIND & 0x80)
      hold_time-=7;//encoder_ctr++;
  else
      hold_time+=7;//encoder_ctr--;    

  encoder_debounce_target = (ms_ctr + ENCODER_DEBOUNCE_TIME) % MS_RESET;

}

// interrupt1: zero crossing
ISR(INT1_vect) // External Interrupt 1
{
  // shift out PWM values
    // ********  TODO: change LED_PORT/DATA to PWM_PORT/DATA in updatepwm: ******** 
  UpdatePWMandHouse();
  // reset couter? probably not- hardware reset works well enough, I think

  // clear interrupt flag just in case (might not be needed)
#ifdef __AVR_ATmega16__
  GIFR = 0x80;
#endif
#ifdef __AVR_ATmega164P__
  EIFR = 0x02;
#endif

}

// interrupt2: tap tempo
ISR(INT2_vect) // External Interrupt 2 (tap tempo)
{

#ifdef __AVR_ATmega16__
  GICR &= ~0x20; // Disable INT2 Interrupt
#endif
#ifdef __AVR_ATmega164P__
  EIMSK &= ~0x04;  // disable INT2 Interrupt
#endif

  // Set hold_time to the current value of the tap timer
  hold_time = tap_ctr;
  // reset tap and hold timers
  tap_ctr  = 0;
  hold_ctr = 0;
  tap_debounce_target = (ms_ctr + TAP_DEBOUNCE_TIME) % MS_RESET;

}
