/*
  Title:    Georgia Tech CCF Light Control Board
  Authors:  Andrew Falendysz and Bill Burke
  Date:     6/2009
  Purpose:  Control the brightness of up to 8 incandescent stage
            lights as well as on-off control of 4 house lights
  Software: AVR-GCC
  Hardware: ATmega16, external hardware as noted in ./Main.sch
  Note:     Interface visual available as ./FrontPanel.pdf
*/

#define TESTMODE

#include "dimmer.h"

//////////  Function Prototypes //////////
void Initialize();
void ScanFaders();
void HandleButtons();
u08  CheckFaders();
void SetDimmers();
void SetRelays();

////////// Variable Definitions //////////

//--- For display ----
u08 leds[3]; // see dimmer.h
u08 blink_leds[3]; // 1=blink 0=value of leds var
u08 blink_value  = 0;
struct preview_status preview; 

//--- For button polling ---
extern u08 buttons[4];
extern u08 last_buttons[4];

//--- Scenes and Sequences ---
struct scene live_scene, xfade_scene, preview_xfade_scene;
struct scene saved_scenes[NUMBER_OF_SCENES];
struct sequence saved_sequences[NUMBER_OF_SEQUENCES];

//--- For Global System State (button handlers) ---
u08 system_state;
u08 return_state; 
u08 current_output_scene = 0;
u08 next_output_scene = 1;

volatile u08 dummy = 0;

//--- For global ms interrupt/count ---
u08 ms_ctr       = 0;

//--- For transition between scenes ---
u08 transition_speed    = 1;
u08 trans_speed_ctr     = 0;
u08 transition_progress = 0;
u08 xfade_active        = 0;

//--- For tap tempo interrupt ---
extern u16 tap_ctr;
extern u08 tap_debounce_target;

//--- For tap tempo led blinking ---
extern u16 hold_ctr;
extern u16 hold_time;
extern u08 hold_LED; 

//--- For rotary encoder interrupt ---
extern u08 encoder_debounce_target;

//testing
u08 result = 0;
u08 input  = 0;
u08 tmp    = 0;

//////////       Main Loop      //////////
int main( void )
{
  Initialize();

  leds[4] = 0xCC;

  //=== for testing PWM ===
  return_state = SEQUENCE;
  saved_scenes[0].faders[0] = 0x00;
  saved_scenes[0].faders[1] = 0x3B;
  saved_scenes[0].faders[2] = 0xF3;
  saved_scenes[0].faders[3] = 0x92;
  saved_scenes[0].faders[4] = 0x14;
  saved_scenes[0].faders[5] = 0x7F;
  saved_scenes[0].faders[6] = 0x50;
  saved_scenes[0].faders[7] = 0x20;
  saved_scenes[0].faders_mute = 0x55;
  saved_scenes[0].house = 0x02;
  saved_scenes[1].faders[0] = 0x7F;
  saved_scenes[1].faders[1] = 0xAA;
  saved_scenes[1].faders[2] = 0x00;
  saved_scenes[1].faders[3] = 0x00;
  saved_scenes[1].faders[4] = 0x7F;
  saved_scenes[1].faders[5] = 0x7F;
  saved_scenes[1].faders[6] = 0xBF;
  saved_scenes[1].faders[7] = 0xFF;
  saved_scenes[1].faders_mute = 0xFF;
  saved_scenes[1].house = 0x01;
  current_output_scene = 0;
  next_output_scene = 1;
  transition_speed = 20; // should result in ~1.3 sec transition

  //preview.mode = PREVIEW_MODE_XFADE;
  preview.mode = PREVIEW_MODE_OFF; 

  live_scene.faders[0] = 0;
  live_scene.faders_mute = 0x71;
  live_scene.house = 0x0F;
  //=======================

  for (;;) 
    {
#ifdef TESTMODE
      
      if(ScanButtons())
	{
	  HandleButtons();
	  //PORTA ^= 0x40;
	  //blink_leds[SCENE_LEDS] = ~blink_leds[SCENE_LEDS];
	}

      //PORTA = (PORTA & 0xEF) | (hold_LED & 0x10); // blink PORTA bit 5 to show hold time

#endif
      DisplayDriver();
      ADCSRA |= 0x40; // Start an A/D conversion
      _delay_ms(8);
      //PORTD ^= 0x01;
      
    }
}


////////// Handle button presses //////////
void HandleButtons()
{
  switch( system_state ){

  case LIVE:
    btn_live_scene_seq_handler();
    break;
    
  case SCENE:
    btn_live_scene_seq_handler();
    break;
    
  case SEQUENCE:
    btn_live_scene_seq_handler();
    break;

  case SET_INIT:
    btn_set_init_handler();
    break;
    
  case SET_SEQA:
    btn_set_seqa_handler();
    break;
    
  case SET_SEQB:
    btn_set_seqb_handler();
    break;
    
  case PREVIEW_INIT:
    btn_preview_init_handler();
    break;
    
  case PREVIEW_SEQ:
    btn_preview_scene_seq_handler();
    break;
    
  case PREVIEW_SCENE:
    btn_preview_scene_seq_handler();    
    break;
    
  default:
    //uh oh....
    break;
  }
  
  return;
}




void Initialize( void )
{
  u08 i;
#ifdef TESTMODE
  // initialize all leds to on
  leds[SCENE_LEDS] = 0xff;

  //blink_leds[SCENE_LEDS] = 0xaa;
  live_scene.faders[7]=0;
#endif
  
#ifdef __AVR_ATmega16__
  TCCR0  |= 0x0B; // Timer0: No PWM, Counter Compare, clock/64
  TIMSK  |= 0x02; // Timer Interrupt Mask: Timer0 overflow enabled (all others disabled)
  OCR0    = 125;  // (8MHz / 64 prescale) / 125 = 1ms per interrupt
  // see pg 86 in ATmega16 datasheet

  ADMUX  |= 0x20; // A/D Converter: AREF reference, Left-aligned, start with AIN1
  ADCSRA |= 0x88; // A/D enabled, interrupt enabled, A/D clk = (sys clk)/2

  MCUCR  |= 0x0E; // INT0 Trigger on falling edge, INT1 on rising edge
  GICR   |= 0xE0; // Enable INT2, INT0 Interrupt (falling edge by default)
#endif
#ifdef __AVR_ATmega164P__
  TCCR0A |= 0x02; // Timer0: No PWM, Counter Compare, clock/64
  TCCR0B |= 0x03; // Timer0: No PWM, Counter Compare, clock/64
  TIMSK0 |= 0x02; // Timer Interrupt Mask: Timer0 overflow enabled (all others disabled)
  OCR0A   = 125;  // (8MHz / 64 prescale) / 125 = 1ms per interrupt
  // see pg 107 in ATmega164p datasheet

  ADMUX  |= 0x20; // A/D Converter: AREF reference, Left-aligned, start with AIN1
  ADCSRA |= 0x88; // A/D enabled, interrupt enabled, A/D clk = (sys clk)/2

  EICRA  |= 0x2E; // INT0, INT2 Trigger on falling edge, INT1 on rising edge
  EIMSK  |= 0x07; // Enable all three external interrupts

  CLKPR = 0x80;   // turn off the clock prescaler (which is div by 8 by default)
  CLKPR = 0x00;   // =0x80 makes it writeable (for next 4 clocks) and =0x00 clears it
#endif

  PORTA = 0x00;
  PORTB = 0x04;
  PORTC = 0x00;
  PORTD = 0x84;

  DDRA  = 0x00;
  DDRB  = 0x0B;   
  DDRC  = 0xFF;
  DDRD  = 0x72;

  // start in LIVE 
  system_state = LIVE;
  return_state = LIVE;

  //initialize button states
  for (i=0; i<4; i++)
    {
      buttons[i] = 0;
      last_buttons[i] = 0;
    }
  
  sei(); // enable global interrupts
}

ISR(TIMER0_COMPA_vect) // Timer0 Compare Match Interrupt Handler
{
  u08 i;

  // This ISR runs every millisecond! :-)

  if ( xfade_active )
    {
      if (trans_speed_ctr >= transition_speed)
	{
	  transition_progress += 3;
	  trans_speed_ctr = 0;
	}
      else
	trans_speed_ctr++;

      if (transition_progress >= 253)
	{  // set 2nd scene to be the current scene
	  // ************* JUST TOGGLES BETWEEN 1 AND 0 FOR TESTING ***********************
	  current_output_scene = next_output_scene;//1 - current_output_scene;
	  //next_output_scene = 1 - next_output_scene;
	  xfade_scene.house = saved_scenes[current_output_scene].house;
	  xfade_scene.faders_mute = saved_scenes[current_output_scene].faders_mute;
	  trans_speed_ctr = 0; 
	  transition_progress = 0;
	  xfade_active = 0;
	  if(system_state == LIVE)
	    preview.mode = PREVIEW_MODE_OFF;
	  else
	    preview.mode = PREVIEW_MODE_SCENE; 
	}
    }

  tap_ctr++;
      
  if (ms_ctr == tap_debounce_target)
    {
#ifdef __AVR_ATmega16__
      GIFR = 0x20;    // Clear any waiting interrupts
      GICR |= 0x20;   // Enable INT2 Interrupt
#endif
#ifdef __AVR_ATmega164P__
      EIFR = 0x04;    // Clear any waiting INT2 interrupts
      EIMSK |= 0x04;  // Enable INT2 Interrupt
#endif
      // make sure we won't do this again until the ISR makes us
      tap_debounce_target = SAFE_VAL; 
    }

  if (hold_ctr < hold_time)
    {
      if (hold_ctr == 0)  // This is just to blink the LED under the encoder
	hold_LED = 0x00;
      else
	if (hold_ctr == HOLD_BLINK_TIME)
	  hold_LED = 0xFF;
      hold_ctr++;
    }
  else
    {
      hold_ctr = 0;
      // this is also where a scene transition would happen
      xfade_active = 1;
    }

  if ((ms_ctr % BLINK_TIME) == 0)
    {
      blink_value = ~blink_value;

#ifdef TESTMODE
      PORTA = (PORTA & 0x7F) | (blink_value & 0x80);
#endif

    }

  if (ms_ctr == encoder_debounce_target)
    {
      encoder_debounce_target = SAFE_VAL;
#ifdef __AVR_ATmega16__
      GIFR =  0x40;
      GICR |= 0x40; //clear and enable INT0 interrupt
#endif
#ifdef __AVR_ATmega164P__
      EIFR = 0x01;    // Clear any waiting INT0 interrupts
      EIMSK |= 0x01;  // Enable INT0 Interrupt
#endif
    }

  ms_ctr++;

  if (ms_ctr >= MS_RESET)
    {
      // we don't let this run through at MS_RESET
      // b/c anything that mods MS_RESET will get 0 
      // for that iteration and also for the subsequent
      // 0th iteration.  This is the only way it works.
      ms_ctr = 0;
    }

}
