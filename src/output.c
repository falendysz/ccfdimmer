#include "dimmer.h"


////////// Variable Definitions //////////

//--- Scenes and Sequences ---
extern struct scene live_scene, xfade_scene;
extern struct scene saved_scenes[NUMBER_OF_SCENES];
extern struct sequence saved_sequences[NUMBER_OF_SEQUENCES];

//--- For Global System State (button handlers) ---
extern u08 system_state;
extern u08 return_state; 
extern u08 current_output_scene;
extern u08 next_output_scene;

//--- For Crossfader
extern u08 transition_progress;
extern u08 xfade_active;


void UpdatePWMandHouse(){
  
  u08 i, ones, bit;
  u08 asm_temp;
  struct scene* output_scene;
  
  //--- Let's see what we're supposed to output ---
  if( xfade_active ){
    output_scene = &(xfade_scene);
    //--- do the heavy crossfading math ---
    for (i=0; i<NUMBER_OF_FADERS; i++)
      {
	//xfade_scene.faders[i] = ((saved_scenes[current_output_scene].faders[i]*(255-transition_progress))>>8)+((saved_scenes[next_output_scene].faders[i]*transition_progress)>>8);
 
	asm volatile(
		     "ldi %[temp], 0xff\n\t"       
		     "sub %[temp], %[trans]\n\t"   
		     "mul %[cur],  %[temp]\n\t"    
		     "mov %[temp], r1\n\t"        
		     "mul %[next], %[trans]\n\t"   
		     "add %[temp], r1\n\t"        
		     "mov %[xfade], %[temp]\n\t"   
		     "clr r1\n\t"                  
		     : [temp]  "=&d" (asm_temp), 
		       [xfade] "=r" (xfade_scene.faders[i])
		     : [trans] "r" (transition_progress), 
		       [cur]   "r" (saved_scenes[current_output_scene].faders[i]), 
		       [next]  "r" (saved_scenes[next_output_scene].faders[i])
		     );
      }
  }
  else
    switch( return_state )
      {      
      case LIVE:
	output_scene = &(live_scene);
	break;
      case SCENE:
      case SEQUENCE:
	output_scene = &(saved_scenes[current_output_scene]);
	break;
      }
  
  /*
  //--- send out the faders to the PWMs ---  
  for(i=0; i<NUMBER_OF_FADERS; i++){
  
    //--- output sequence leds ---
    // ********  TODO: change LED_PORT/DATA to PWM_PORT/DATA  ******** 
    ones = output_scene->faders[i];
    
    for(bit=0; bit<8; bit++){
      if (ones % 2)
	LED_PORT |= LED_DATA;
      else
	LED_PORT &= ~LED_DATA;
      LED_PORT |= LED_CLK;
      LED_PORT &= ~LED_CLK;
      ones = ones >> 1;
    }
    
  }
  */
  
  //--- update the house lights ---
  // *** TODO: THIS STOMPS ON OUR TESTING LEDs, SO IT IS BEING DISABLED FOR NOW ***
  //RELAY_PORT = output_scene->house;

}
