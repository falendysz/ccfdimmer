#ifndef DIMMER_H
#define DIMMER_H

//--- running at 8MHz ---
#define F_CPU      8000000UL

#include <io.h>
#include <util/delay.h>
#include <interrupt.h>


typedef unsigned char  u08;
typedef unsigned int   u16;

//#############################################
// Board Attributes
//#############################################
#define NUMBER_OF_FADERS 8
#define NUMBER_OF_SCENES 8
#define NUMBER_OF_SEQUENCES 4
#define NUMBER_OF_HOUSE_CTLS 4
#define MAX_SCENES_PER_SEQUENCE 8


//#############################################
// Data Structures
//#############################################

// scene structure
struct scene{
  
  //--- faders ---
  u08 house;                    // leds and values (on/off)
  u08 faders[NUMBER_OF_FADERS]; // full resolution fader values
  u08 faders_mute;              // for each fader channel (assumes 8 channels!)
  
};

// sequence structure 
struct sequence{

  u08 scene_queue[MAX_SCENES_PER_SEQUENCE];
  u08 scene_count;
  u16 scene_hold_time;

};


//##############################################
// Timer Constants
//##############################################
// master reset value for counter (least common multiple
// of everything that will trigger off a mod operation)
#define MS_RESET          250
// SAFE_VAL must be > MS_RESET and within data type bounds
#define SAFE_VAL          251
#define BLINK_TIME        250
#define HOLD_BLINK_TIME   50
#define TAP_DEBOUNCE_TIME 150
#define ENCODER_DEBOUNCE_TIME 4


// RELAY port
//????????????????????????????????????
#define RELAY_PORT PORTA


//################################################
/////////////////// Global System States ///////
// these states are used to control the 
// flow of the program.  We only poll certain
// buttons in certain states, and our reactions
// are determined by the state we're in.

// WYS is WYG states 
#define LIVE            0
#define SCENE           1
#define SEQUENCE        2

// after hitting set button from either
// LIVE or SCENE
#define SET_INIT        3

// set -> seq# gets you here.  it's still
// possible to exit (by pressing set) without
// making any changes
#define SET_SEQA        4

// set->seq#->scene# gets you here.
// now you're defining the sequence, and it
// will be written when set is pressed again
#define SET_SEQB        5

// after hitting preview button from either
// LIVE or SCENE
#define PREVIEW_INIT    6

// preview->seq# gets you here.  this shows what
// the sequence will do on the faders with the
// appropriate timing.  You can hit another seq
// or scene to preview it.  exits back to LIVE/SCENE
// when the sequence is done
#define PREVIEW_SEQ     7

// preview->scnene# gets you here.  displays 
// scene on faders until timeout.  you can select 
// a different scene or sequence before the timeout
// expires if you want.
#define PREVIEW_SCENE   8


//###############################################
/////////////////// Display Driver //////////////
#define PREVIEW_MODE_OFF      0x00
#define PREVIEW_MODE_SCENE    0x01
#define PREVIEW_MODE_SEQUENCE 0x02
#define PREVIEW_MODE_XFADE    0x03

struct preview_status{
  
  u08 mode; // see defines above
  u08 sequence_index;
  u08 scene_index;

};

// For Reference...
//struct scene{
//  u08 house;        // leds and values (on/off)
//  u08 faders[8];    // full resolution fader values
//  u08 mute;         // for each fader channel  
//};

//leds: 
//    blackouts   x 2       *  %
//    scenes      x 8       $  %
//    sequences   x 4       $  %
//    mode        x 2       *  %
//    preview     x 1       *  %
//    set         x 1       *  %
//    bargraphs   x (8*10)  &
//    fader mutes x 8x2     &
//    house       x 4x2     &
//    tap tempo   x 1       $
//          
// * = always live 
// & = determined by scene being displayed
// $ = set externally 
// % = goes into led register (18 at last count)

//LED variable
#define ALWAYS_LIVE_LEDS 0x00
// 2 blackouts, 2 modes, preview, set 
#define NUM_ALWAYS_LIVE_LEDS 6 
#define SCENE_LEDS       0x01
#define SEQUENCE_LEDS    0x02

#define MODE_SCENE_LED   0x08
#define MODE_LIVE_LED    0x04
#define PREVIEW_LED      0x02
#define SET_LED          0x01
#define BLACKOUT_STAGE_LED
#define BLACKOUT_HOUSE_LED
// etc....

// LED Port
#define LED_PORT   PORTD
#define LED_CLK    0x40
#define LED_DATA   0x20


//###############################################
/////////////////// Buttons /////////////////////

// Button Port
#define BTN_SELECT PORTB
#define BTN_READ   ~(PINB)

// Button buffer offsets
#define SCENES 0
#define MISC   1
#define STAGE  2
#define HOUSE  3
#define MISC2  4

// Scene bitmasks
#define SCENE1 0x01
#define SCENE2 0x02
#define SCENE3 0x04
#define SCENE4 0x08
#define SCENE5 0x10
#define SCENE6 0x20
#define SCENE7 0x40
#define SCENE8 0x80

// Fader bitmasks
#define FADER1 0x01
#define FADER2 0x02
#define FADER3 0x04
#define FADER4 0x08
#define FADER5 0x10
#define FADER6 0x20
#define FADER7 0x40
#define FADER8 0x80

// House Button bitmasks
#define HOUSE1_ON  0x01
#define HOUSE2_ON  0x02
#define HOUSE3_ON  0x04
#define HOUSE4_ON  0x08
#define HOUSE1_OFF 0x10
#define HOUSE2_OFF 0x20
#define HOUSE3_OFF 0x40
#define HOUSE4_OFF 0x80

//////////// MISC byte ////////
// Sequence Bitmasks
#define SEQUENCES 0x0F
#define SEQUENCE1 0x01
#define SEQUENCE2 0x02
#define SEQUENCE3 0x04
#define SEQUENCE4 0x08

// Misc Bitmasks
#define MODE_BUTTON    0x40
#define PREVIEW_BUTTON 0x20
#define SET_BUTTON     0x10
#define BLACKOUT_HOUSE 0x80

///////// MISC2 Byte ////////////

#define BLACKOUT_STAGE 0x01

#endif
