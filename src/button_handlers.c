//------- Button Handlers for all states. -----

#include "dimmer.h"

////////// Variable Definitions //////////
extern u08 leds[5];
extern u08 blink_leds[5];
extern u08 bargraphs[4];
extern u08 faders[8];
extern u08 buttons[4];
extern u08 last_buttons[4];

extern u08 current_output_scene;
extern u08 next_output_scene;
extern u08 xfade_active;
extern u08 transition_progress;
extern struct scene saved_scenes[NUMBER_OF_SCENES];

extern struct preview_status preview;

extern u08 system_state;
extern u08 return_state; 

extern struct scene live_scene;


// Used for filling in sequence on the fly
u08 temp_sequence[MAX_SCENES_PER_SEQUENCE];
u08 temp_scene_count;
u08 temp_target;
// shadow variable names to reuse them
// for another purpose
#define temp_scene_index temp_scene_count
#define temp_preview_seq temp_target



//###################################
// Functions
//###################################

inline u08 MSB_Asserted(u08 a){

  if(a >= 0x80)
    return 7;
  if(a >= 0x40)
    return 6;
  if(a >= 0x20)
    return 5;
  if(a >= 0x10)
    return 4; 
  if(a >= 0x08)
    return 3; 
  if(a >= 0x04)
    return 2; 
  if(a >= 0x02)
    return 1;
  if(a >= 0x01)
    return 0;
  else
    return 255;
}


//--- Live Mode, Scene Mode, and Sequence Mode are the same
void btn_live_scene_seq_handler(){
  u08 i = 0;
    
  if (buttons[MISC] & MODE_BUTTON)
    {
      if(system_state == LIVE){
	system_state = SCENE;
	return_state = SCENE;
	// set scene led
	leds[ALWAYS_LIVE_LEDS] |= MODE_SCENE_LED;
	leds[ALWAYS_LIVE_LEDS] &= ~MODE_LIVE_LED;
      }
      else{
	preview.mode = PREVIEW_MODE_OFF;
	system_state = LIVE;
	return_state = LIVE;
	// set live led
	leds[SCENE_LEDS] = 0;
	leds[ALWAYS_LIVE_LEDS] |= MODE_LIVE_LED;
	leds[ALWAYS_LIVE_LEDS] &= ~MODE_SCENE_LED;
      }
    }
  if (buttons[SCENES])           // scene buttons were pressed
    {
      //#### immediately begin crossfade to selected scene
      preview.mode = PREVIEW_MODE_XFADE;
      next_output_scene = MSB_Asserted(buttons[SCENES]);
      transition_progress = 0;
      xfade_active = 1;
      leds[SCENE_LEDS] = 0x80 >> next_output_scene;
      leds[ALWAYS_LIVE_LEDS] |= MODE_SCENE_LED;
      leds[ALWAYS_LIVE_LEDS] &= ~MODE_LIVE_LED;
      system_state = SCENE;
      return_state = SCENE;
    }
  if (buttons[MISC] & SET_BUTTON)       // set
    {
      system_state = SET_INIT;
      //#### set "set" led
      blink_leds[ALWAYS_LIVE_LEDS] |= SET_LED;
      leds[ALWAYS_LIVE_LEDS] |= SET_LED;
      //#### blink scenes and sequences
      leds[SCENE_LEDS] = 0xff;
      leds[SEQUENCE_LEDS] |= 0x0f;
      blink_leds[SCENE_LEDS] = 0xff;
      blink_leds[SEQUENCE_LEDS] |= 0x0f;
    }
  if (buttons[MISC] & SEQUENCES) // sequences
    {
      next_output_scene = MSB_Asserted(buttons[SCENES]);
      leds[SEQUENCE_LEDS] = next_output_scene;
      transition_progress = 0;
      xfade_active = 1;
      leds[SCENE_LEDS] = 0x80 >> next_output_scene;
      leds[ALWAYS_LIVE_LEDS] |= MODE_SCENE_LED;
      leds[ALWAYS_LIVE_LEDS] &= ~MODE_LIVE_LED;
      system_state = SEQUENCE;
      return_state = SEQUENCE;
      //#### set sequence led
      //#### initialize sequence 
    }
  if (buttons[HOUSE])            // house lights
    {
      // house relay control bits should be
      // R4 R3 R2 R1 PWM PWM PWM PWM
      
      // House on buttons are in the lower nib
      // House off buttons are in the upper nib
      // relay pins are upper nib of the port.

      //!!!!!! EDIT !!!!!-----------------------------
      //#### set live 
      for (i=0; i<8; i+=2)
	{
	  live_scene.house &= ~((~buttons[HOUSE]) & (1 << i)) >> i;
	  live_scene.house |= ((buttons[HOUSE]) & (1 << (i+1))) >> (i+1);
	}    
    }
  if (buttons[MISC] & BLACKOUT_HOUSE)
    {                           
      //#### blackout house
      
    }
  if (buttons[MISC] & BLACKOUT_STAGE)
    {                           
      //#### blackout stage
      
    }
  if (buttons[STAGE])            
    {
      //#### stage mute
    }

  return;
}


void btn_set_init_handler(){
  u08 scene_to_save = 0;
  u08 i = 0;

  blink_leds[ALWAYS_LIVE_LEDS] &= ~SET_LED;
  blink_leds[SCENE_LEDS]  =  0x00;
  blink_leds[SEQUENCE_LEDS] &= ~0x0f;
  leds[ALWAYS_LIVE_LEDS] &= ~SET_LED;
  leds[SCENE_LEDS]  =  0x00;
  leds[SEQUENCE_LEDS] &= ~0x0f;
      
  if( buttons[SCENES] ){

    //#### stop blinking scenes and sequences

    // we just need to set the scene and 
    // return to the calling mode

    //#### copy faders and house to MSB_Asserted(buttons[SCENES])
    scene_to_save = MSB_Asserted(buttons[SCENES]);
    for(i=0; i<NUMBER_OF_FADERS;i++)
      {
	saved_scenes[scene_to_save].faders[i] = live_scene.faders[i];
      }
    
    saved_scenes[scene_to_save].house = live_scene.house;
    
    system_state = return_state;

    return;
  }

  if( buttons[MISC] & SEQUENCES ){
    //#### stop blinking sequences
    
    //#### blink scenes and tap
    
    temp_target = MSB_Asserted(buttons[MISC] & SEQUENCES);

    system_state = SET_SEQA;
    return;
  }

  if( buttons[MISC] & SET_BUTTON ){
    
    //#### stop blinking scenes and sequences

    // quit without setting anything
    system_state = return_state;
    return;
  }

}


void btn_set_seqa_handler(){

  if( buttons[SCENES] ){
    // add the scene to the queue
    // we know it's the first one
    temp_sequence[0] = MSB_Asserted(buttons[SCENES]);
    temp_scene_count=1;

    //#### turn on scene's light

    // move on to SET_SEQB
    system_state = SET_SEQB;
    return;
  }

  if( buttons[MISC] & SET_BUTTON ){
    //#### stop blinking scenes and sequences

    // quit without setting anything
    system_state = return_state;
    return;
  }
}

void btn_set_seqb_handler(){

  if( buttons[SCENES] ){

    // add the scene to the queue
    temp_sequence[temp_scene_count] = MSB_Asserted(buttons[SCENES]);
    temp_scene_count++;

    //#### turn on scene's light
  }

  if( (buttons[MISC] & SET_BUTTON) || 
      (temp_scene_count >= MAX_SCENES_PER_SEQUENCE) ){
      
      //#### write temp_sequence to target
      //#### stop blinking sc and tap

      // return to calling state
      system_state = return_state;
      return;
  }
   
}

void btn_preview_init_handler(){
  
  if( buttons[SCENES] ){
    
    //#### display scene
    system_state = PREVIEW_SCENE;
    return;
  }
  
  if( buttons[MISC] & SEQUENCES ){
    temp_preview_seq = MSB_Asserted(buttons[MISC] & SEQUENCES);
    temp_scene_index = 0;

    //#### setup scene display timeout (maybe stored
    //     as hold time)
   
    //#### display scene
    system_state = PREVIEW_SEQ;
  }

}
    

void btn_preview_scene_seq_handler(){
  
  // assume that the timeout is handled 
  // elsewhere.

  if( buttons[MISC2] & PREVIEW_BUTTON ){
    // exit preview

    //#### revert display
    
    system_state = return_state;
    return;
  }


  if( buttons[SCENE] ){
        
    //#### display scene
    system_state = PREVIEW_SCENE;
    return;
  }

  if( buttons[MISC] & SEQUENCES ){
    temp_preview_seq = MSB_Asserted(buttons[MISC] & SEQUENCES);
    temp_scene_index = 0;

    //#### setup scene display timeout (maybe stored
    //     as hold time)
   
    //#### display scene
    system_state = PREVIEW_SEQ;
  }
}

