/*
  Title:    AVR-GCC test program #1 for the STK200 eva board
  (modified by Rolf Johansson <rojo (at) nocrew.org>)
  Author:   Volker Oth
  Date:     4/1999
  Purpose:  Flashes the LEDs on Port B with a hard coded delay loop
  needed
  Software: AVR-GCC
  needed
  Hardware: ATS90S8515/8535/2313/mega on STK200/STK300 board
  Note:     To contact me, mail to
  volkeroth (at) gmx.de
  You might find more AVR related stuff at my homepage:
  http://members.xoom.com/volkeroth
*/

#include <io.h>

typedef unsigned char  u08;

int main( void )
{
  u08 led, i, j, k, l, dc, run, rundc;

  DDRB = 0xFF;            /* use all pins on PortB for output */

  led = 1;                  /* init variable representing the LED state */
  dc = 1;                     /* Init the direction (dc) */
  run = 1;
  rundc = 0;
   
  for (;;) {
    PORTB = ~led;      /* invert the output since a zero means: LED on */
    if (dc==0)
      led >>= 1;              /* move to next LED */
    if (dc==1)
      led <<= 1;              /* move to next LED */
    if (!led) {             /* overflow: start with Port B0 again */
      if (dc == 0) {
	led = 1;
	dc = 1;
      }
      else {
	led = 128;
	dc = 0;
      }
      if (rundc == 0)
	run++;
      if (rundc == 1)
	run--;
    }
    if (run == 1)
      rundc = 0;
    if (run == 255)
      rundc = 1;
    for (i=0; i<run; i++)     /* outer delay loop */
      for (j=0; j<run;j++)   /* inner delay loop */
	for (l=0; l<16;l++) /* inner inner delay loop */
	  k++;                /* just do something - could also be a NOP */
  }
}
